package dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import excepciones.ConnectionException;

public class ConexionFile {

	private static final String PATH_FILE = "bdclientes.bd";
	private static RandomAccessFile raf = null;

	public static RandomAccessFile getConexion() throws ConnectionException {
		if (raf == null) {
			try {
				raf = new RandomAccessFile(PATH_FILE, "rw");
			} catch (FileNotFoundException e) {
				throw new ConnectionException("Error al intentar abrir el fichero: " + e.getMessage());
			}
		}
		return raf;
	}

	public static void cerrar() throws ConnectionException {
		if (raf != null) {
			try {
				raf.close();
				raf = null;
			} catch (IOException e) {
				throw new ConnectionException("Error al intentar cerrar el fichero: " + e.getMessage());
			}

		}
	}

}
